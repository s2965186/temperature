import nl.utwente.di.bookQuote.Quoter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestQuoter {
    @Test
    public void testBook1() throws Exception{
        Quoter quoter = new Quoter();
        double temp = quoter.getFarenheit("0");
        Assertions.assertEquals(32, temp);
    }
}
