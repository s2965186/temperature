package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    public static double getFarenheit(String temp){
        double x = Double.parseDouble(temp);
        return (x*9/5)+32;
    }
}
